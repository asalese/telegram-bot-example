require 'spec_helper'
require_relative '../app/tv/movie'

describe Movie do
  subject(:movie) { described_class.new('Barbie', 'Won 1 Oscar. 193 wins & 420 nominations total') }

  describe 'model' do
    it { is_expected.to respond_to(:awards) }
  end
end
