require 'spec_helper'
require_relative '../app/tv/movies_client'
require 'debug'

def stub_omdb_movie_client(awards = 'Won 11 Oscars. 126 wins & 83 nominations total', body = nil)
  api_body = { "Title": 'Titanic',
               "Awards": awards }

  api_body = body if body

  stub_request(:get, 'http://www.omdbapi.com/?apikey&t=Titanic')
    .with(
      headers: {
        'Accept' => '*/*',
        'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
        'User-Agent' => 'Faraday v2.7.4'
      }
    )
    .to_return(status: 200, body: api_body.to_json, headers: {})
end

describe MoviesClient do
  describe 'movie' do
    it 'should return a movie when movie exists' do
      stub_omdb_movie_client
      expect(described_class.movie('Titanic')).to be_a(Movie)
    end

    it 'should return a movie with nil awards when movie has none' do
      stub_omdb_movie_client('N/A')
      expect(described_class.movie('Titanic').awards).to be_nil
    end

    it 'should return nil when movie does not exist' do
      stub_omdb_movie_client('', { "Response": 'False', "Error": 'Movie not found!' })
      expect(described_class.movie('Titanic')).to be_nil
    end
  end
end
