class MoviesClient
  API_URL = 'http://www.omdbapi.com/'.freeze
  API_KEY = ENV['OMDB_API_KEY']
  NO_AWARDS = 'N/A'.freeze

  def self.movie(movie_name)
    response = Faraday.get(API_URL, { t: movie_name, apikey: API_KEY })
    response_json = JSON.parse(response.body)
    return nil if response_json['Response'] == 'False'

    awards = response_json['Awards']
    awards = awards == NO_AWARDS ? nil : response_json['Awards']
    name = response_json['Title']
    Movie.new(name, awards)
  end
end
