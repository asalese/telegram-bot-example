class Movie
  attr_reader :awards

  def initialize(name, awards)
    @name = name
    @awards = awards
  end
end
